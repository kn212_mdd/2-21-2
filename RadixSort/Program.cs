﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace RadixSort
{
    class Program
    {
        static void Sort(int[] arr)
        {
            int i, j;
            int[] tmp = new int[arr.Length];
            for (int zrush = 31; zrush > -1; --zrush)
            {
                j = 0;
                for (i = 0; i < arr.Length; ++i)
                {
                    bool move = (arr[i] << zrush) >= 0;
                    if (zrush == 0 ? !move : move)
                        arr[i - j] = arr[i];
                    else
                        tmp[j++] = arr[i];
                }
                Array.Copy(tmp, 0, arr, arr.Length - j, j);
            }

        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.InputEncoding = System.Text.Encoding.Unicode;
            
            Console.Write("Введіть кількість чисел в масиві = ");
            int n = Convert.ToInt32(Console.ReadLine());
            Random random = new Random();

            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
            {
                arr[i] = random.Next(-100, 100);
            }
            Console.WriteLine("\nПочатковий масив: ");
            for (int i = 0; i < n; i++)
            {
                Console.Write("{0}\t", arr[i]);
            }
            Sort(arr);
            Console.WriteLine("\nВідсортований масив: ");
            for (int i = 0; i < n; i++)
            {
                Console.Write("{0}\t", arr[i]);
            }
        
            Console.ReadKey();
        }
    }
}
